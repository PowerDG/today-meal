---
title: 葱香牛肉
categories: 厨艺笔谈
tags: 菜谱
date: 2022/04/19 18:49:00
update: 2022/04/19 18:49:00
cover: https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220419200824.JPG
---

# 葱香牛肉

## 食材

### 主料

- 牛里脊

### 辅料

- 洋葱
- 小葱
- 大葱
- 姜蒜
- 香菜



## 准备工作

### 牛肉

**1、切片**

牛肉切薄片

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220419185244.png" alt="CleanShot 2022-04-19 at 18.52.37@2x" width="25%" />

**2、码味去腥**

切少许葱姜蒜香菜，加些微胡椒粉，一点点盐，一勺鸡精，少量料酒，用力抓挤出葱姜水备用。

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220419194333.png" alt="CleanShot 2022-04-19 at 19.43.27@2x" width="25%" />

牛肉中加少许嫩肉粉，抓匀后加入用力抓挤出的葱姜水，蛋清一个，搅拌至肉吸收完水分，腌制备用。

**3、上浆**

此步骤可以在其他准备工作完成后进行，牛肉片加入少量玉米淀粉，抓拌均匀

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220419194705.png" alt="CleanShot 2022-04-19 at 19.46.58@2x" width="25%" />



### 洋葱与葱花

**1、洋葱切二粗丝**

洋葱大半个，剥皮切丝

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220419194832.png" alt="CleanShot 2022-04-19 at 19.48.26@2x" width="25%" />

**2、葱花**

小葱一大把切葱花

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220419194921.png" alt="CleanShot 2022-04-19 at 19.49.15@2x" width="25%" />



### 碗芡

少量淀粉，少量水，一勺鸡精，一勺味精，搅拌均匀。



## 开始制作

### 炒洋葱

> 1. 炙锅
> 2. 锅中少许油，下洋葱翻炒，翻炒的时候加一点点盐给一点底味
> 3. 炒熟后捞出装盘垫底

### 炒牛肉

> 1. 炙锅
> 2. 炙锅后锅中加宽油，冷油下牛肉，用筷子缓慢滑散
> 3. 牛肉炒变色后即可捞出备用

洗锅

> 1. 再炙锅
> 2. 炙锅后加小半勺油，下姜蒜片翻炒，加入适量辣鲜露（大概生抽的分量），少量味极鲜酱油。
> 3. 下牛肉翻炒上色，然后下碗芡**大火**稍微收汁即可出锅盛于洋葱之上。
> 4. 牛肉上面撒大量葱花

洗锅

> 1. 烧一小勺热油淋在葱花上，再点缀少许生葱花即可。

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220419200757.png" alt="CleanShot 2022-04-19 at 20.07.47@2x" width="25%" />

### 最终成品

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220419200824.JPG" alt="IMG_2239" width="45%" />

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220419200844.jpeg" alt="IMG_2236" width="45%" />