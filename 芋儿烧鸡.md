---
title: 芋儿烧鸡
categories: 厨艺笔谈
tags: 菜谱
date: 2022/04/06 18:11:00
update: 2022/04/06 18:11:00
cover: https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220406185119.JPG
---



# 芋儿烧鸡

## 原料

- 鲜鸡半只
- 毛芋头1-2斤
- 葱姜蒜
- 豆瓣酱
- 火锅底料（可选，不能吃辣就别加）

- 香料

  - 八角

    <img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220406181646.jpeg" alt="74f93849094192a239f9bf6b0f79606e" width="20%" />

  - 白芷

    <img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220406181738.jpeg" alt="6efc5ef9073732eb31d4b7727d057506" width="20%" />

  - 山奈

    <img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220406181916.jpeg" alt="5fbac85b5b0ca8290a81025c26fedffa" width="20%" />

  - 香叶

    <img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220406181955.jpeg" alt="f1e77f42fa0ac418b8113e3432ac9a98" width="20%" />

  - 桂皮

    <img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220406182101.jpeg" alt="fff4eabc13200819aeebce866dcfa6c9" width="20%" />

  - 花椒



## 准备工作

### 准备辅料

姜蒜剁粒，大葱切段备用

### 鸡肉砍块焯水

**1、鸡肉砍块**

鲜鸡砍成适当的块，不宜太小，然后抓洗干净，洗至没有血水后放入盆中备用。

**2、焯水**

冷水下鸡肉开大火煮，期间加入半瓶盖白酒去腥，煮出浮沫后撇去浮沫，随后将鸡肉捞出洗净。鸡肉焯水洗净后如下：

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220406182458.png" alt="截屏2022-04-06 18.24.55" width="45%" />

### 芋头处理

芋头刮皮洗净后切块，比鸡肉块略大。

### 准备香料

准备八角、白芷、山奈、桂皮、香叶少许，花椒一小把备用，备好后如图：

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220406182728.png" alt="截屏2022-04-06 18.27.25" width="45%" />







## 开始制作

>1. 炙锅
>2. 下鸡肉炒，然后下香料，姜蒜粒炒
>3. 炒到鸡肉比较干，水汽较少后下豆瓣酱（口味重能吃辣可以另加一些火锅底料），下辣椒面适量提色
>4. 豆瓣酱炒出香味后加水没过鸡肉开始煮
>5. 随后加入白糖两勺和味提鲜，加入胡椒粉小勺，料酒适量，下入葱段
>6. 小火炖煮至鸡肉软烂（此过程时间较长，芋头的处理可以放在此时）
>
>7. 鸡肉软烂后下入芋头，加入盐适量（一勺左右），如果水较少还可加入适量清水
>8. 同时把锅内的香料、葱姜等捡出不要
>9. 小火焖煮至芋头软烂（不要大火煮，焖煮更入味好吃）（小火焖煮所需时间较长，可以换成把鸡肉芋头放入高压锅中上汽煮2-3分钟关火闷十分钟）
>10. 芋头焖熟焖软后关火起锅装盘，可点缀葱花适量



## 最终成品

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220406185119.JPG" alt="IMG_2095" width="45%" />

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220406185138.jpeg" alt="IMG_2094" width="45%" />